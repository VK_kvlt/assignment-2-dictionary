%include "lib.inc"

%define BUFFER_SIZE 256
%define STDOUT 1
%define STDERR 2

global _start

section .data
	%include "words.inc"
	not_found_msg db "String not found!", 0

section .text
	_start:
		sub rsp, BUFFER_SIZE

		xor rax, rax
		xor rdi, rdi
		mov rsi, rsp
		mov rdx, 255
		syscall
		mov byte [rsp+rax], 0

		call print_newline

		mov rdi, rsp
		mov rsi, top_elem
		call find_word

		or rax, rax
		jz not_found
		lea rdi, [rax+8]
		mov rdi, qword [rdi]
		call print_string
		call print_newline
		xor rdi, rdi
		jmp exit

		not_found:
		mov rax, STDOUT
		mov rdi, STDERR
		mov rsi, not_found_msg
		mov rdx, 17
		syscall

		call print_newline

		mov rdi, STDOUT
		jmp exit

		%include "dict.asm"


		
