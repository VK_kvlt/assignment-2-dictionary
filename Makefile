ASM=nasm
ASMFLAGS=-f elf64
LINKER=ld


PHONY: all
all: main

main.o: main.asm dict.asm m words.inc colon.inc Makefile
	$(ASM) $(ASMFLAGS) $< -o $@

lib.o: lib.asm Makefile
	$(ASM) $(ASMFLAGS) $< -o $@

main: lib.o main.o
	$(LINKER) $? -o $@
